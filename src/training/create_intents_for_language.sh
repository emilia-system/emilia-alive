#!/bin/bash

language="$1"
# TODO check for the argument.

for _number in $(seq 1 $(wc -l < intent_list.txt)); do
    line=$(head -n $_number intent_list.txt | tail -n 1)
    mkdir -p intents/${line%\/*}   # get the folder path and create it.
    touch intents/$line-$language.txt  # create the file. Touch is used if by accident the file already exists.
done
