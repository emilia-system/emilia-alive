#!/usr/bin/env python

import configparser  # For ini files.
import os
import sys

# Go to the script path
os.chdir(os.path.dirname(sys.argv[0]))

# Open the configuration file
config = configparser.ConfigParser()
config.read('../training.conf')
assistant_name   = config["Persona"]["name"]
assistant_female = config["Persona"]["is_female"]

# Helping little tools.
def pre_training_is_process_needed(source_file,generated_file):
    # Check modification date of both files. If no change is there,
    #  Skip generation.
    if os.path.isfile(generated_file):
        return (os.path.getmtime(source_file) > 
            os.path.getmtime(generated_file))
    else:
        # Generated file doesn't exist. Let the function create it
        return True
# Script itself

def pre_training_expand_line(line):
    # Change:
    #  [name], if present.
    #  if last digit is not '?', add a dot at the end.
    
    # Strip and replace the pronoun. In portuguese there is words like sir that only adds the female part, and not replace.
    if assistant_female == "true":
        line = line.strip().replace("[ao]","a").replace("[a]","a").replace("[o]","")
    else:
        line = line.strip().replace("[ao]","o").replace("[a]","").replace("[o]","o")
    # Replace the name if manually added.
    line = line.replace("[name]",assistant_name)
    
    # If someone putted a dot at the end, remove it.
    if line[-1] == '.':
        line = line[:-1]
    # TODO, if at the end it's a '?', remove it and re-add at the end
    
    # Change accentuation 
    change_a = ["á","ã","à"]
    change_e = ["é"]
    change_i = ["í"]
    change_o = ["ó","õ"]
    change_u = ["ú"]
    for letter in change_a:
        line = line.replace(letter,"a")
    for letter in change_e:
        line = line.replace(letter,"e")
    for letter in change_i:
        line = line.replace(letter,"i")
    for letter in change_o:
        line = line.replace(letter,"o")
    for letter in change_u:
        line = line.replace(letter,"u")
    
    # Default is lower case.
    line = line.lower()
    name_lower = assistant_name.lower()

    # Return list of 5 lines:
    #  The new processed string
    #  The same line with "<name> " before.
    #  The same line with " <name>" at the end.
    #  The 2 above but with a ',' separating.
    return [line + ".",
            name_lower + " "  + line + ".",
            name_lower + ", " + line + ".",
            line + " "  + name_lower + ".",
            line + ", " + name_lower + "."
            ]

def pre_training_expand_file(file_path):
    if not pre_training_is_process_needed(
        file_path, file_path.rstrip(".txt") + "_treated.txt"):
        print(" - File " + file_path + " Expanded already. Skipping")
        return True
    with open(file_path,"r") as raw_data:
        print(" Expanding File " + file_path + "...")
        with open(file_path.rstrip(".txt") + "_treated.txt","w") as semi_raw_data:
            # For every line in the original file...
            for line in raw_data:
                # Write the results of the training in the new file
                # separeted by a \n, and finishing with one at the end.
                semi_raw_data.write(
                    "\n".join(
                        pre_training_expand_line(line)) + "\n")
    return True
def pre_training_start(lang):    
    with open("../intent_list.txt","r") as file_list:
        for file_path in file_list:
            pre_training_expand_file(file_path.rstrip("\n") + "-" + lang + ".txt")

#def semi_training_tokenize():
    

pre_training_start("pt_BR")
